/*
 * eth-echo-server.h
 *
 *  Created on: Nov 14, 2020
 *      Author: mircodemarchi
 *
 * Echo Server header file.
 */

#ifndef INC_ETH_ETH_ECHO_SERVER_H_
#define INC_ETH_ETH_ECHO_SERVER_H_

/* --- Includes ------------------------------------------------------------- */
/* Application includes. */
#include "eth.h"

/* --- Constants ------------------------------------------------------------ */
/** @brief Task server listening and server instance stack size. */
#define echo_serverTASK_STACK_SIZE		(configMINIMAL_STACK_SIZE + 100)
/** @brief Task server listening and server instance priority. */
#define echo_serverTASK_PRIORITY		(tskIDLE_PRIORITY + 1)

/** @brief Receive timeout in millisecond. */
#define echo_serverRX_TIMEOUT_MS												\
	(ipconfigSOCK_DEFAULT_RECEIVE_BLOCK_TIME 	* portTICK_PERIOD_MS)
/** @brief Send timeout in millisecond. */
#define echo_serverTX_TIMEOUT_MS												\
	(ipconfigSOCK_DEFAULT_SEND_BLOCK_TIME 		* portTICK_PERIOD_MS)
/** @brief Shutdown delay in millisecond. */
#define echo_serverSHUTDOWN_TIME_MS				5000

/** @brief Receive buffer size in bytes. */
#define echo_serverRX_BUFFER_SIZE				(ipconfigTCP_RX_BUFFER_LENGTH)
/** @brief Send buffer size in bytes. */
#define echo_serverTX_BUFFER_SIZE				(ipconfigTCP_TX_BUFFER_LENGTH)
/** @brief Echo received message size. */
#define echo_serverMESSAGE_SIZE					(ipconfigTCP_MSS)

/** @brief Enable semaphore in socket. */
#define echo_serverWITH_SEMAPHORE				0

/** @brief Receive sliding window size in MSS. */
#define echo_serverRX_SLIDING_WIN_MSS			2
/** @brief Send sliding window size in MSS. */
#define echo_serverTX_SLIDING_WIN_MSS			2

/** @brief Server port. */
#define echo_serverPORT_NUMBER					3000

/** @brief Limit of client connected. */
#define echo_serverMAX_CONNECTION				10

/** @brief Separator for task name. */
#define echo_serverNAME_SEPARATOR				"::"
/** @brief Listening task identification name. */
#define echo_serverNAME_LISTEN_TASK				"Listen"

/* -------------------------------------------------------------------------- */

/**
 * @brief Echo Server task creation.
 * @param pvParameters Parameter not used for this task.
 * @return pdPASS if task created successfully, pdFAIL if not enough memory to
 * create a new task.
 */
BaseType_t vEchoServerCreateTask(void *pvParameters);

#endif /* INC_ETH_ETH_ECHO_SERVER_H_ */
