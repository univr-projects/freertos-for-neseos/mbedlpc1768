/*
 * eth.h
 *
 *  Created on: Oct 25, 2020
 *      Author: mircodemarchi
 *
 * Ethernet TCP and UDP Socket creation and setup header file.
 */

#ifndef INC_ETH_ETH_H_
#define INC_ETH_ETH_H_

/* --- Includes ------------------------------------------------------------- */
/* Application includes. */
#include "eth-common.h"
/* -------------------------------------------------------------------------- */

/**
 * @brief Initialize Ethernet stack.
 *
 * If ipconfigUSE_NETWORK_EVENT_HOOK is set to 1 in FreeRTOSIPConfig.h then the
 * TCP/IP stack will call the vIPNetworkEventHook() callback function when the
 * network is ready for use.
 *
 * See vIPNetworkEventHook() in FreeRTOSHooks.c source file.
 */
void vEthInit();

/**
 * @brief Create a TCP socket and setup it.
 * @param xRxTimeoutMS Receive timeout in millisecond.
 * @param xTxTimeoutMS Send timeout in millisecond.
 * @param lRxBufferSize Receive buffer size in byte.
 * @param lTxBufferSize Send buffer size in byte.
 * @param pxSem Pointer to a semaphore to include in socket.
 * @param lRxSlidingWinMSS Receive sliding window size in MSS unit.
 * @param lTxSlidingWinMSS Send sliding window size in MSS unit.
 * @return The socket created.
 *
 * Setup performed:
 * - FREERTOS_SO_RCVTIMEO: Receive timeout.
 * - FREERTOS_SO_SNDTIMEO: Send timeout.
 * - FREERTOS_SO_SET_SEMAPHORE: Semaphore for the socket if
 * ipconfigSOCKET_HAS_USER_SEMAPHORE is 1.
 * - FREERTOS_SO_WIN_PROPERTIES: buffers and sliding windows size.
 * - FREERTOS_SO_RCVBUF: receive buffer size if FREERTOS_SO_WIN_PROPERTIES
 * setup failed.
 * - FREERTOS_SO_SNDBUF: send buffer size if FREERTOS_SO_WIN_PROPERTIES
 * setup failed.
 */
Socket_t xEthCreateTCPSocket(TickType_t xRxTimeoutMS ,TickType_t xTxTimeoutMS,
		int32_t lRxBufferSize, int32_t lTxBufferSize,
		SemaphoreHandle_t *pxSem,
		int32_t lRxSlidingWinMSS, int32_t lTxSlidingWinMSS);

/**
 * @brief Create a TCP socket and setup it.
 * @param xRxTimeoutMS Receive timeout in millisecond.
 * @param xTxTimeoutMS Send timeout in millisecond.
 * @param xEnableChecksum Enable or disable checksum.
 * @param ulRxPacketNum Set the maximum number of received packet in queue.
 * @return The socket created.
 *
 * Setup performed:
 * - FREERTOS_SO_RCVTIMEO: Receive timeout.
 * - FREERTOS_SO_SNDTIMEO: Send timeout.
 * - FREERTOS_SO_UDPCKSUM_OUT: Enable or disable checksum in UDP packet and if
 * disabled the checksum will always set to 0.
 * - FREERTOS_SO_UDP_MAX_RX_PACKETS: maximum number of packet in receive queue
 * if ipconfigUDP_MAX_RX_PACKETS is 1.
 */
Socket_t xEthCreateUDPSocket(TickType_t xRxTimeoutMS ,TickType_t xTxTimeoutMS,
		BaseType_t xEnableChecksum, BaseType_t ulRxPacketNum);

/**
 * @brief Setup a socket with receive and send timeout.
 * @param xSocket Socket to apply the setting.
 * @param xRxTimeoutMS Receive timeout in millisecond.
 * @param xTxTimeoutMS Send timeout in millisecond.
 * @return pdPASS if setting applied, pdFAIL if something went wrong.
 *
 * Setup performed:
 * - FREERTOS_SO_RCVTIMEO: Receive timeout.
 * - FREERTOS_SO_SNDTIMEO: Send timeout.
 */
BaseType_t bEthSetRxTxTimeout(Socket_t xSocket, TickType_t xRxTimeoutMS,
		TickType_t xTxTimeoutMS);

/**
 * @brief Setup a socket with a semaphore referenced.
 * @param xSocket Socket to apply the setting.
 * @param pxSem Pointer to a semaphore to include in socket.
 * @return pdPASS if setting applied, pdFAIL if something went wrong.
 *
 * Setup performed:
 * - FREERTOS_SO_SET_SEMAPHORE: Semaphore for the socket if
 * ipconfigSOCKET_HAS_USER_SEMAPHORE is 1.
 */
BaseType_t bEthSetSemaphore(Socket_t xSocket, SemaphoreHandle_t *pxSem);

/**
 * @brief Setup a socket with receive and send buffer size.
 * @param xSocket Socket to apply the setting.
 * @param lRxBufferSize Receive buffer size in byte.
 * @param lTxBufferSize Send buffer size in byte.
 * @return pdPASS if setting applied, pdFAIL if something went wrong.
 *
 * Setup performed:
 * - FREERTOS_SO_RCVBUF: receive buffer size if ipconfigUSE_TCP_WIN is 0.
 * - FREERTOS_SO_SNDBUF: send buffer size if ipconfigUSE_TCP_WIN is 0.
 */
BaseType_t bEthSetRxTxBuffSize(Socket_t xSocket,
		int32_t lRxBufferSize, int32_t lTxBufferSize);

/**
 * @brief Setup a socket with receive and send buffer size and sliding window
 * size.
 * @param xSocket Socket to apply the setting.
 * @param lRxBufferSize Receive buffer size in byte.
 * @param lTxBufferSize Send buffer size in byte.
 * @param lRxSlidingWinMSS Receive sliding window size in MSS unit.
 * @param lTxSlidingWinMSS Send sliding window size in MSS unit.
 * @return pdPASS if setting applied, pdFAIL if something went wrong.
 *
 * Setup performed:
 * - FREERTOS_SO_WIN_PROPERTIES: buffers and sliding windows size if
 * ipconfigUSE_TCP_WIN is 1.
 */
BaseType_t bEthSetRxTxWinBuffSize(Socket_t xSocket,
		int32_t lRxBufferSize, int32_t lTxBufferSize,
		int32_t lRxSlidingWinMSS, int32_t lTxSlidingWinMSS);

/**
 * @brief Setup a socket with checksum generation enabled or disabled.
 * @param xSocket Socket to apply the setting.
 * @param xEnableChecksum Enable or disable checksum.
 * @return pdPASS if setting applied, pdFAIL if something went wrong.
 *
 * Setup performed:
 * - FREERTOS_SO_UDPCKSUM_OUT: Enable or disable checksum in UDP packet and if
 * disabled the checksum will always set to 0.
 */
BaseType_t bEthSetChecksumGeneration(Socket_t xSocket,
		BaseType_t xEnableChecksum);

/**
 * @brief Setup a socket with maximum number of packet in receive queue.
 * @param xSocket Socket to apply the setting.
 * @param ulRxPacketNum Set the maximum number of received packet in queue.
 * @return pdPASS if setting applied, pdFAIL if something went wrong.
 *
 * Setup performed:
 * - FREERTOS_SO_UDP_MAX_RX_PACKETS: maximum number of packet in receive queue
 * if ipconfigUDP_MAX_RX_PACKETS is 1.
 */
BaseType_t bEthSetMaxRxPacket(Socket_t xSocket, BaseType_t ulRxPacketNum);

/**
 * @brief Stop received data (only valid for TCP sockets).
 * @param xSocket Socket to apply the setting.
 * @return pdPASS if setting applied, pdFAIL if something went wrong.
 *
 * Setup performed:
 * - FREERTOS_SO_STOP_RX: forces the socket to advertise a window of zero,
 * enabling the socket to temporarily stop receiving data.
 */
BaseType_t bEthDisableRxData(Socket_t xSocket);

/**
 * @brief Enable received data (only valid for TCP sockets).
 * @param xSocket Socket to apply the setting.
 * @return pdPASS if setting applied, pdFAIL if something went wrong.
 *
 * Setup performed:
 * - FREERTOS_SO_STOP_RX: the socket should use its default behavior.
 */
BaseType_t bEthEnableRxData(Socket_t xSocket);

/**
 * @brief Stop received data (only valid for TCP sockets).
 * @param xSocket Socket to apply the setting.
 * @return pdPASS if setting applied, pdFAIL if something went wrong.
 *
 * By default a listening socket will create a new socket to handle any
 * accepted connections.
 *
 * Setup performed:
 * - FREERTOS_SO_REUSE_LISTEN_SOCKET: change this behavior so accepted
 * connections are handled by the listening socket itself, therefore, after
 * socket connection accept, the application stop to listen for other
 * connections. After FreeRTOS_closesocket() a new socket can be created and
 * bound to the same port.
 */
BaseType_t bEthReuseListening(Socket_t xSocket);

/**
 * @brief Stop received data (only valid for TCP sockets).
 * @param xSocket Socket to apply the setting.
 * @return pdPASS if setting applied, pdFAIL if something went wrong.
 *
 * Setup performed:
 * - FREERTOS_SO_CLOSE_AFTER_SEND TCP: close a socket immediately after the
 * last data has been delivered. Before calling FreeRTOS_send() for the last
 * time, set this option, so the stack knows that the last packet must include
 * the FIN flag.
 */
BaseType_t bEthCloseOnLastDataSent(Socket_t xSocket);

/**
 * @brief Stop received data (only valid for TCP sockets).
 * @param xSocket Socket to apply the setting.
 * @return pdPASS if setting applied, pdFAIL if something went wrong.
 *
 * Setup performed:
 * - FREERTOS_SO_SET_FULL_SIZE: not send any data from the socket until
 * there is at least one complete MSS size of data ready to be sent. This
 * option can be used to improve performance, but make sure the option is
 * switched off on the last send, so that the last bytes (less than MSS) will
 * also be delivered.
 */
BaseType_t bEthSendOnlyFullPacket(Socket_t xSocket);

/**
 * @brief Socket graceful close.
 * @param xSocket Socket to close.
 * @param xShutdownDelayMS Time waited for socket graceful close.
 * @return pdTRUE if the socket has been closed graceful, pdFALSE if forced.
 */
BaseType_t bEthGracefulClose(Socket_t xSocket, TickType_t xShutdownDelayMS);

/**
 * @brief Bind socket to port.
 * @param xSocket Socket to bind.
 * @param usPort Port to set.
 * @return pdPASS if binded successfully, pdFAIL if something went wrong during
 * bind process.
 */
BaseType_t bEthBind(Socket_t xSocket, uint16_t usPort);

/**
 * @brief Connect socket to IP address and port.
 * @param xSocket Socket to connect.
 * @param usPort Port to set.
 * @param pcIP	 String of IP address to set.
 * @return pdPASS if connected successfully, pdFAIL if something went wrong
 * during connection process.
 */
BaseType_t bEthConnect(Socket_t xSocket, uint16_t usPort, char *pcIP);

/**
 * @brief Transmit data through a socket.
 * @param xSocket Socket to send data.
 * @param pcBuffer Buffer to send.
 * @param ulLength Length of the buffer.
 * @param pulBytesTransmitted Pointer of a variable to fill with the length of
 * the data actually transmitted.
 * @return pdPASS if transferred successfully, pdFAIL if an error occurred
 * during transmission process.
 */
BaseType_t bEthTx(Socket_t xSocket, const uint8_t *pcBuffer, size_t ulLength,
		size_t *pulBytesTransmitted);

/**
 * @brief Receive data through a socket.
 * @param xSocket Socket to receive data.
 * @param pcBuffer Buffer to fill with received data.
 * @param ulLength Length of the buffer.
 * @param pulBytesReceived Pointer of a variable to fill with the length of
 * the data actually received.
 * @return pdPASS if received successfully or the receiving timeout exceeded,
 * pdFAIL if an error occurred during the receipt process.
 *
 * If timeout exceeded, then the pulBytesReceived pointer value is set to 0.
 */
BaseType_t bEthRx(Socket_t xSocket, uint8_t *pcBuffer, size_t ulLength,
		size_t *pulBytesReceived);

#endif /* INC_ETH_ETH_H_ */
