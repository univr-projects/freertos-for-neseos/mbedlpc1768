/*
 * echo-client.h
 *
 *  Created on: Nov 14, 2020
 *      Author: mircodemarchi
 *
 * Echo Client header file.
 */

#ifndef INC_ETH_ECHO_CLIENT_H_
#define INC_ETH_ECHO_CLIENT_H_

/* --- Includes ------------------------------------------------------------- */
/* Application includes. */
#include "eth.h"

/* --- Constants ------------------------------------------------------------ */
/** @brief Task server listening and server instance stack size. */
#define echo_clientTASK_STACK_SIZE		(configMINIMAL_STACK_SIZE + 100)
/** @brief Task server listening and server instance priority. */
#define echo_clientTASK_PRIORITY		(tskIDLE_PRIORITY + 1)

/** @brief Receive timeout in millisecond. */
#define echo_clientRX_TIMEOUT_MS												\
	(ipconfigSOCK_DEFAULT_RECEIVE_BLOCK_TIME 	* portTICK_PERIOD_MS)
/** @brief Send timeout in millisecond. */
#define echo_clientTX_TIMEOUT_MS												\
	(ipconfigSOCK_DEFAULT_SEND_BLOCK_TIME 		* portTICK_PERIOD_MS)
/** @brief Shutdown delay in millisecond. */
#define echo_clientSHUTDOWN_TIME_MS				5000

/** @brief Receive buffer size in bytes. */
#define echo_clientRX_BUFFER_SIZE				(ipconfigTCP_RX_BUFFER_LENGTH)
/** @brief Send buffer size in bytes. */
#define echo_clientTX_BUFFER_SIZE				(ipconfigTCP_TX_BUFFER_LENGTH)
/** @brief Echo received message size. */
#define echo_clientMESSAGE_SIZE					(ipconfigTCP_MSS)

/** @brief Enable semaphore in socket. */
#define echo_clientWITH_SEMAPHORE				0

/** @brief Receive sliding window size in MSS. */
#define echo_clientRX_SLIDING_WIN_MSS			2
/** @brief Send sliding window size in MSS. */
#define echo_clientTX_SLIDING_WIN_MSS			2

/** @brief Server port. */
#define echo_clientSERVER_PORT_NUMBER			3000

/** @brief Echo client task wait time on each iteration in millisecond. */
#define echo_clientLOOP_DELAY_MS				150

/* -------------------------------------------------------------------------- */

/**
 * @brief Echo Client task creation.
 * @param pvParameters Parameter for Echo Client task, that in this case is the
 * Echo Server IP address.
 * @return pdPASS if task created successfully, pdFAIL if not enough memory to
 * create a new task.
 */
BaseType_t vEchoClientCreateTask(void *pvParameters);



#endif /* INC_ETH_ECHO_CLIENT_H_ */
