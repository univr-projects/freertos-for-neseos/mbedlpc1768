/*
 * eth-common.h
 *
 *  Created on: Oct 25, 2020
 *      Author: mircodemarchi
 *
 * Ethernet common data and utils header file.
 */

#ifndef INC_ETH_COMMON_H_
#define INC_ETH_COMMON_H_

/* --- Includes ------------------------------------------------------------- */
/* Standard includes. */
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

/* Application includes. */
#include "common.h"

/* Kernel includes. */
#include "FreeRTOS_IP.h"
#include "FreeRTOS_Sockets.h"

/* --- Constants ------------------------------------------------------------ */
/**
 * @brief Define names that will be used for DNS, LLMNR and NBNS searches.
 * This allows mainHOST_NAME to be used when the IP address is not known.
 * For example "ping ProjectNESEOS" to resolve ProjectNESEOS to an IP address
 * then send a ping request.
 */
#define eth_commonHOST_NAME					"ProjectNESEOS"
#define eth_commonDEVICE_NICK_NAME			"mbedlpc1768"

/** @brief Size of MAC address in bytes. */
#define eth_commonMAC_ADDRESS_LEN			6

/** @brief Size of IP address in byte. */
#define eth_commonIP_ADDRESS_LEN			4
/** @brief Size of IP address in string form "XXX.XXX.XXX.XXX". */
#define eth_commonIP_ADDRESS_STR_LEN		((3 * eth_commonIP_ADDRESS_LEN) + 4)

/* --- Structures ----------------------------------------------------------- */
/**
 * @brief Connected Client Socket Info.
 */
typedef struct EthCommonData {
	/** @brief Socket of connected client. */
	Socket_t xSocket;
	/** @brief Socket port and IP address informations. */
	struct freertos_sockaddr xSocketInfo;
	/** @brief IP address in string. */
	char *pcIPStr;
} EthCommonData_t;

/* --- Globals -------------------------------------------------------------- */
extern uint8_t ucMACAddress[];				/**< @brief MAC address. */
extern const uint8_t ucIPAddress[];			/**< @brief IP address. */
extern const uint8_t ucNetMask[];			/**< @brief Net Mask IP address. */
extern const uint8_t ucGatewayAddress[];	/**< @brief Gateway IP address. */
extern const uint8_t ucDNSServerAddress[];	/**< @brief DNS address. */

/* -------------------------------------------------------------------------- */

/**
 * @brief Convert the IP address in string with the format "XXX.XXX.XXX.XXX".
 * @param pcBuffer Buffer in which write the IP address converted in string.
 * @param ulIP 	   The IP address to convert.
 * @return The same buffer pointer in input.
 */
char *pcEthCommonIpToStr(char *pcBuffer, uint32_t ulIP);

/**
 * @brief Initialize the EthCommonData_t structure with connection client
 * connected data info.
 * @param xSocket The Socket connected.
 * @param xSocketInfo IP address and port of the client connected.
 * @return Pointer to an allocated EthCommonData_t structure with the connected
 * client informations.
 */
EthCommonData_t *vEthCommonInitData(Socket_t xSocket,
		struct freertos_sockaddr xSocketInfo);

/**
 * @brief Deallocate an EthCommonData_t structure.
 * @param pxData Pointer to the EthCommonData_t structure.
 */
void vEthCommonDeinitData(EthCommonData_t *pxData);

#endif /* INC_ETH_COMMON_H_ */
