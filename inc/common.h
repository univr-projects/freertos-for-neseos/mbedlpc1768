/*
 * common.h
 *
 *  Created on: Nov 14, 2020
 *      Author: mircodemarchi
 *
 * Application common.
 */

#ifndef INC_COMMON_H_
#define INC_COMMON_H_

/* --- Includes ------------------------------------------------------------- */
/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

/* --- Constants ------------------------------------------------------------ */
#define commonTASK_ECHOSERVER_NAME 	"EchoServer"
#define commonTASK_ECHOCLIENT_NAME 	"EchoClient"
#define commonTASK_UNKNOWN_NAME 	"Unknown"

/* --- Structures ----------------------------------------------------------- */
typedef enum Task
{
	TASK_ECHOSERVER,
	TASK_ECHOCLIENT
} Task_t;
/* -------------------------------------------------------------------------- */

inline static const char *pcCommonTaskString(Task_t eTaskType)
{
	switch (eTaskType)
	{
		case TASK_ECHOSERVER:
		{
			return commonTASK_ECHOSERVER_NAME;
		}
		case TASK_ECHOCLIENT:
		{
			return commonTASK_ECHOCLIENT_NAME;
		}
		default:
		{
			return commonTASK_UNKNOWN_NAME;
		}
	}
}

#endif /* INC_COMMON_H_ */
