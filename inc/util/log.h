/*
 * log.h
 *
 *  Created on: May 12, 2020
 *      Author: mircodemarchi
 *
 * Log utility.
 */

#ifndef LOG_H_
#define LOG_H_

/* ----- Includes ----------------------------------------------------------- */
/* Standard includes. */
#include <stdio.h>

/* ----- Structures --------------------------------------------------------- */
enum {
	LOG_LVL_NONE, 	  	// 0
	LOG_LVL_ERROR, 		// 1
	LOG_LVL_WARNING,  	// 2
	LOG_LVL_INFO,      	// 3
	LOG_LVL_DEBUG, 		// 4
	LOG_LVL_MAX 		// 5
};

/* ----- Constants ---------------------------------------------------------- */
#ifndef LOG_LEVEL
#define LOG_LEVEL LOG_LVL_INFO
#endif

/* ----- Globals ------------------------------------------------------------ */
#if DEBUG
static const char logLvlToString[][6] = {
	"     ",
	"ERROR",
	"WARN ",
	"INFO ",
	"DEBUG"
};
#endif
/* -------------------------------------------------------------------------- */

#define LOG_SHOULD_DISPLAY( level ) 											\
	( level <= LOG_LEVEL && level < LOG_LVL_MAX)

#define LOG_HELPER(level, fmt, args...) do { 									\
	if (LOG_SHOULD_DISPLAY(level)) {											\
		printf("%s %s:%-4d [%s()] " fmt "\r\n",           			 			\
				logLvlToString[level], __FILE__, __LINE__, __func__, ## args);	\
	} 																			\
} while(0)

#if DEBUG
#define LOG(fmt, args...)  LOG_HELPER(LOG_LVL_NONE   , fmt, ## args)
#define LOGE(fmt, args...) LOG_HELPER(LOG_LVL_ERROR  , fmt, ## args)
#define LOGW(fmt, args...) LOG_HELPER(LOG_LVL_WARNING, fmt, ## args)
#define LOGI(fmt, args...) LOG_HELPER(LOG_LVL_INFO   , fmt, ## args)
#define LOGD(fmt, args...) LOG_HELPER(LOG_LVL_DEBUG  , fmt, ## args)
#else
#define LOG(fmt, args...)
#define LOGE(fmt, args...)
#define LOGW(fmt, args...)
#define LOGI(fmt, args...)
#define LOGD(fmt, args...)
#endif

#endif /* LOG_H_ */
