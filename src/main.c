/*
 * main.c
 *
 *  Created on: Oct 25, 2020
 *      Author: mircodemarchi
 */

/* --- Includes ------------------------------------------------------------- */
/* Standard includes. */
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>

/* Application includes. */
#include "LPC17xx.h"
#include "lpc17xx_emac.h"
#include "eth.h"

/* Kernel includes. */
#include "FreeRTOS.h"
#include "task.h"

/* --- Globals -------------------------------------------------------------- */

/**
 * @brief The libraries use large data arrays. Place these manually in the
 * AHB RAM so their combined size is not taken into account when calculating
 * the total flash+RAM size of the generated executable.
 */
uint8_t *pucFreeRTOSHeap = NULL;

/**
 * @brief The emac buffers are manually placed at the start of the AHB RAM.
 * These variables store the calculated addresses, which are then made
 * available through the pvApplicationGetEMACTxBufferAddress() and
 * pvApplicationGetEMACRxBufferAddress() access functions.
 */
static uint32_t ulEMACTxBufferStart = 0UL, ulEMACRxBufferStart = 0UL;
/* -------------------------------------------------------------------------- */

static void prvManuallyPlaceLargeDataInAHBRAM( void )
{
	uint32_t ulAddressCalc;
	extern uint32_t __top_RamAHB32[];
	const uint32_t ulBaseAddress = 0x2007c000UL;

	/* Start at the bottom of the second bank of RAM.  Need to use a linker
	variable here! */
	ulAddressCalc = ulBaseAddress;
	ulEMACTxBufferStart = ulAddressCalc;

	/* Move up far enough to hold all the Tx buffers. */
	ulAddressCalc += sizeof(xEMACTxBuffer_t);

	/* Align and store the calculated address. */
	ulAddressCalc += 0x0fUL;
	ulAddressCalc &= ~0x0fUL;
	ulEMACRxBufferStart = ulAddressCalc;

	/* Move up far enough to hold all the Rx buffers. */
	ulAddressCalc += sizeof(xEMACRxBuffer_t);

	/* Align and assign the calculated address to the FreeRTOS heap. */
	ulAddressCalc += 0x0fUL;
	ulAddressCalc &= ~0x0fUL;
	pucFreeRTOSHeap = (uint8_t *) ulAddressCalc;

	/* Sanity check that the variables placed in AHB RAM actually fit in AHB
	RAM before zeroing down the memory. */
	configASSERT((ulAddressCalc + configTOTAL_HEAP_SIZE)
			     < (uint32_t) __top_RamAHB32);
	memset((void *) ulBaseAddress, 0x00,
			(ulAddressCalc + configTOTAL_HEAP_SIZE) - ulBaseAddress);

	/* Remove compiler warnings when configASSERT() is not defined. */
	(void) __top_RamAHB32;
}

/**
 * @brief Sets up system hardware
 */
static void prvSetupHardware(void)
{
	SystemCoreClockUpdate();

	prvManuallyPlaceLargeDataInAHBRAM();
}
/* -------------------------------------------------------------------------- */

/**
 * @brief	Main routine for LPC1768 Network Embedded System and Advanced
 * Operating System project.
 * @return	Nothing, function should not exit.
 */
int main(void)
{
	LOGD("Firmware start");

	/* Setup Hardware. */
	prvSetupHardware();

	/* Initialize Ethernet task. */
	LOGD("Ethernet initialization");
	vEthInit();

	/* Start the scheduler. */
	vTaskStartScheduler();

	LOGE("Should never arrive here");
	while(1){};
	return 1;
}

/**
 * @brief Simply return the address calculated as being the bottom of the EMAC
 * Tx buffers.
 */
void *pvApplicationGetEMACTxBufferAddress( void )
{
	return ( void * ) ulEMACTxBufferStart;
}

void *pvApplicationGetEMACRxBufferAddress( void )
{
	/* Simply return the address calculated as being the bottom of the emac
	Rx buffers. */
	return ( void * ) ulEMACRxBufferStart;
}
