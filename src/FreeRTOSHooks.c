/*
 * FreeRTOSHooks.c
 *
 *  Created on: Oct 25, 2020
 *      Author: mircodemarchi
 *
 * FreeRTOS callback implementations.
 */

/* --- Includes ------------------------------------------------------------- */
/* Standard includes. */
#include <stdlib.h>
#include <time.h>
#include <string.h>

/* Application includes. */
#include "log.h"
#include "eth-common.h"
#include "echo-server.h"
#include "echo-client.h"

/* Kernel includes. */
#include "FreeRTOS.h"
#include "FreeRTOS_IP.h"
#include "task.h"

/* Use by the pseudo random number generator. */
static UBaseType_t ulNextRand;
/* -------------------------------------------------------------------------- */

/**
 * @brief Run time stack overflow checking is performed if
 * configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2. This hook
 * function is called if a stack overflow is detected.
 */
void vApplicationStackOverflowHook(TaskHandle_t pxTask, char *pcTaskName)
{
	(void) pcTaskName;
	(void) pxTask;
	taskDISABLE_INTERRUPTS();
	for(;;);
}

/**
 * @brief vApplicationMallocFailedHook() will only be called if
 * configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h.  It is a hook
 * function that will get called if a call to pvPortMalloc() fails.
 * pvPortMalloc() is called internally by the kernel whenever a task, queue,
 * timer or semaphore is created.  It is also called by various parts of the
 * demo application.  If heap_1.c or heap_2.c are used, then the size of the
 * heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
 * FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
 * to query the size of free heap space that remains (although it does not
 * provide information on how the remaining heap might be fragmented).
 */
void vApplicationMallocFailedHook()
{
	taskDISABLE_INTERRUPTS();
	for(;;);
}

/**
 * @brief This function configures a timer that is used as the time base when
 * collecting run time statistical information - basically the percentage
 * of CPU time that each task is utilising.  It is called automatically when
 * the scheduler is started (assuming configGENERATE_RUN_TIME_STATS is set
 * to 1).
 */
void vConfigureTimerForRunTimeStats()
{
	const unsigned long TCR_COUNT_RESET = 2, CTCR_CTM_TIMER = 0x00, TCR_COUNT_ENABLE = 0x01;

	/* Power up and feed the timer. */
	LPC_SC->PCONP |= 0x02UL;
	LPC_SC->PCLKSEL0 = (LPC_SC->PCLKSEL0 & (~(0x3<<2))) | (0x01 << 2);

	/* Reset Timer 0 */
	LPC_TIM0->TCR = TCR_COUNT_RESET;

	/* Just count up. */
	LPC_TIM0->CTCR = CTCR_CTM_TIMER;

	/* Prescale to a frequency that is good enough to get a decent resolution,
	but not too fast so as to overflow all the time. */
	LPC_TIM0->PR =  ( configCPU_CLOCK_HZ / 10000UL ) - 1UL;

	/* Start the counter. */
	LPC_TIM0->TCR = TCR_COUNT_ENABLE;
}

/**
 * @brief FreeRTOS application tick hook.
 */
void vApplicationTickHook()
{
	return;
}

/**
 * @brief FreeRTOS application network hook.
 */
void vApplicationIPNetworkEventHook(eIPCallbackEvent_t eNetworkEvent)
{
	static BaseType_t xTasksAlreadyCreated = pdFALSE;

	/* Both eNetworkUp and eNetworkDown events can be processed here. */
	if(eNetworkEvent == eNetworkUp)
	{
		/*
		 * Create the tasks that use the TCP/IP stack if they have not already
		 * been created.
		 */
		if(xTasksAlreadyCreated == pdFALSE)
		{
			/*
			 * For convenience, tasks that use FreeRTOS+TCP can be created here
			 * to ensure they are not created before the network is usable.
			 */

			LOGD("Create EchoServer task");
			vEchoServerCreateTask(NULL);

			/*
			 *  TODO: change the following IP address with the echo server IP
			 *  address that could be your PC in which an echo server
			 *  application started on 3000 port.
			 */
			const char * pcEchoServerIP = "192.168.1.23";
			LOGD("Create EchoClient task on %s server IP", pcEchoServerIP);
			vEchoClientCreateTask((void *) pcEchoServerIP);

			/********* ADD HERE YOUR TASK CREATION *********/

			xTasksAlreadyCreated = pdTRUE;
		}
	}
}

/**
 * @brief Assign the name "ProjectNESEOS" to this network node.
 * This function will be called during the DHCP: the machine will be
 * registered with an IP address plus this name.
 */
const char *pcApplicationHostnameHook( void )
{
	return eth_commonHOST_NAME;
}

/**
 * @brief Determine if a name lookup is for this node. Two names are given
 * to this node: that returned by pcApplicationHostnameHook() and that set
 * by mainDEVICE_NICK_NAME.
 */
BaseType_t xApplicationDNSQueryHook(const char *pcName)
{
	BaseType_t xReturn;

	if(strcasecmp(pcName, pcApplicationHostnameHook()) == 0)
	{
		xReturn = pdPASS;
	}
	else if(strcasecmp(pcName, eth_commonDEVICE_NICK_NAME) == 0)
	{
		xReturn = pdPASS;
	}
	else
	{
		xReturn = pdFAIL;
	}

	return xReturn;
}

/**
 * @brief Called by FreeRTOS+UDP when a reply is received to an outgoing ping
 * request.
 */
void vApplicationPingReplyHook(ePingReplyStatus_t eStatus, uint16_t usIdentifier)
{
	switch( eStatus )
	{
		case eSuccess	:
			LOGD("\r\n\r\nPing reply received - ");
			break;

		case eInvalidChecksum :
			LOGE("\r\n\r\nPing reply received with invalid checksum - ");
			break;

		case eInvalidData :
			LOGE("\r\n\r\nPing reply received with invalid data - ");
			break;

		default :
			/* It is not possible to get here as all enums have their own
			case. */
			break;
	}

	LOGI("identifier %d\r\n\r\n", usIdentifier);
}

/**
 * @brief This xApplicationGetRandomNumber() will set *pulNumber to a random
 * number, and return pdTRUE. When the random number generator is broken, it
 * shall return pdFALSE.
 * The function is defined in 'iot_secure_sockets.c'. If that module is not
 * included in the project, the application must provide an implementation of
 * it.
 */
BaseType_t xApplicationGetRandomNumber(uint32_t *pulValue)
{
	const uint32_t ulMultiplier = 0x015a4e35UL, ulIncrement = 1UL;
	static BaseType_t xInitialised = pdFALSE;

	/* Don't initialise until the scheduler is running, as the timeout in the
	random number generator uses the tick count. */
	if(xInitialised == pdFALSE)
	{
		if(xTaskGetSchedulerState() == taskSCHEDULER_NOT_STARTED)
		{
			return pdFALSE;
		}

		uint32_t ulSeed = time(NULL);
		srand(ulSeed);
		ulNextRand = rand();
		xInitialised = pdTRUE;
	}

	/* Utility function to generate a pseudo random number. */

	ulNextRand = ( ulMultiplier * ulNextRand ) + ulIncrement;
	*pulValue = ((int) ( ulNextRand >> 16UL ) & 0x7fffUL);
	return pdTRUE;
}

/**
 * @brief Callback that provides the inputs necessary to generate a randomized
 * TCP. Initial Sequence Number per RFC 6528. In this case just a psuedo random
 * number is used so THIS IS NOT RECOMMENDED FOR PRODUCTION SYSTEMS.
 */
uint32_t ulApplicationGetNextSequenceNumber(
	uint32_t ulSourceAddress,
    uint16_t usSourcePort,
    uint32_t ulDestinationAddress,
    uint16_t usDestinationPort)
{
     (void) ulSourceAddress;
     (void) usSourcePort;
     (void) ulDestinationAddress;
     (void) usDestinationPort;

     uint32_t ulRet;
     xApplicationGetRandomNumber(&ulRet);
     return ulRet;
}
