/*
 * echo-server.c
 *
 *  Created on: Nov 14, 2020
 *      Author: mircodemarchi
 *
 * Echo Server implementation.
 */


/* --- Includes ------------------------------------------------------------- */
/* Application includes. */
#include "echo-server.h"

/* --- Prototypes ----------------------------------------------------------- */
/**
 * @brief Echo Server task listening for new connections.
 * @param pvParameters Parameter not used for this task.
 */
static void prvvEchoServerListeningTask(void *pvParameters);

/**
 * @brief Echo Server task instance creation.
 * @param pxClientData The connected client data.
 * @return pdPASS if task created successfully, pdFAIL if not enough memory to
 * create a new task.
 */
static BaseType_t prveEchoServerCreateInstanceTask(
		EthCommonData_t * pxClientData);

/**
 * @brief Echo Server task instance of new connection.
 * @param pvParameters Parameter not used for this task.
 */
static void prvvEchoServerInstanceTask(void *pvParameters);

/* --- Globals -------------------------------------------------------------- */
SemaphoreHandle_t xEchoServerSemaphore = NULL;
/* -------------------------------------------------------------------------- */

static void prvvEchoServerListeningTask(void *pvParameters)
{
	struct freertos_sockaddr xClient;
	socklen_t xSize = sizeof(xClient);

#if echo_serverWITH_SEMAPHORE
	xEchoServerSemaphore = xSemaphoreCreateBinary();
#endif

	/* Create and bind socket. */
	LOGD("Socket creation and bind");
	Socket_t xListening = xEthCreateTCPSocket(
			echo_serverRX_TIMEOUT_MS, echo_serverTX_TIMEOUT_MS,
			echo_serverRX_BUFFER_SIZE, echo_serverTX_BUFFER_SIZE,
			&xEchoServerSemaphore, echo_serverRX_SLIDING_WIN_MSS,
			echo_serverTX_SLIDING_WIN_MSS);
	if (xListening == FREERTOS_INVALID_SOCKET
			|| (bEthBind(xListening, echo_serverPORT_NUMBER) != pdPASS))
	{
		LOGE("Impossible to allocate and bind a new socket");
		vTaskDelete(NULL);
		return;
	}

	/* Set the created socket in the listening state. */
	if (FreeRTOS_listen(xListening, echo_serverMAX_CONNECTION) != 0)
	{
		LOGE("Socket not successfully placed into listening state");
		vTaskDelete(NULL);
		return;
	}
	LOGD("Socket listening");

	/* Accept new incoming connections and handle it with a new task. */
	while(1)
	{
		Socket_t xConnectedSocket = FreeRTOS_accept(xListening, &xClient,
				&xSize);

		if (xConnectedSocket == FREERTOS_INVALID_SOCKET)
		{
			LOGE("Invalid socket connection on FreeRTOS_accept");
			continue;
		}

		EthCommonData_t * pxClientData = vEthCommonInitData(xConnectedSocket,
						xClient);
		if (!pxClientData)
		{
			LOGE("Failed allocating memory for connected client data info");
			continue;
		}

		LOGI("Socket accepted on IP address %s and port %d",
				pxClientData->pcIPStr,
				FreeRTOS_ntohs(xClient.sin_port));

		if (prveEchoServerCreateInstanceTask(pxClientData)
				== pdFAIL)
		{
			LOGE("Instance task for client with IP address %s and port %d "
				 "not created", pxClientData->pcIPStr,
				 FreeRTOS_ntohs(xClient.sin_port));
			continue;
		}
	}
}

static BaseType_t prveEchoServerCreateInstanceTask(
		EthCommonData_t * pxClientData)
{
	/* Build task name. */
	size_t pcTaskNameSize = strlen(pcCommonTaskString(TASK_ECHOSERVER))
							+ strlen(echo_serverNAME_SEPARATOR)
							+ strlen(pxClientData->pcIPStr) + 1;
	char *pcTaskName = pvPortMalloc(sizeof(char) * pcTaskNameSize);
	if (!pcTaskName)
	{
		LOGE("Not enough memory for task name allocation");
		return pdFAIL;
	}
	memset(pcTaskName, 0x00, pcTaskNameSize);
	strcpy(pcTaskName, pcCommonTaskString(TASK_ECHOSERVER));
	strcat(strcat(pcTaskName, echo_serverNAME_SEPARATOR),
			pxClientData->pcIPStr);

	if (xTaskCreate(prvvEchoServerInstanceTask,
				pcTaskName,
				echo_serverTASK_STACK_SIZE,
				(void *) pxClientData,
				echo_serverTASK_PRIORITY,
				NULL) != pdPASS)
	{
		LOGE("Not enough memory for %s task", pcTaskName);
		vPortFree(pcTaskName);
		return pdFAIL;
	}

	vPortFree(pcTaskName);
	return pdPASS;
}

static void prvvEchoServerInstanceTask(void *pvParameters)
{
	EthCommonData_t *xConnectedClientData = (EthCommonData_t *) pvParameters;
	LOGD("Created instance task for %s IP address and %d port connection",
		 xConnectedClientData->pcIPStr,
		 FreeRTOS_ntohs(xConnectedClientData->xSocketInfo.sin_port));

	/* Create the buffer used to receive the string to be echoed back. */
	uint8_t *pucRxBuffer = (uint8_t *) pvPortMalloc(echo_serverMESSAGE_SIZE);
	size_t ulRxLength;

	/* Perform echo. */
	if (pucRxBuffer != NULL)
	{
		while(1)
		{
			memset(pucRxBuffer, 0x00, echo_serverMESSAGE_SIZE);

			/* Receive echo. */
			if (bEthRx(xConnectedClientData->xSocket, pucRxBuffer,
					echo_serverMESSAGE_SIZE, &ulRxLength) != pdPASS)
			{
				/* Socket closed. */
				LOGW("Socket with IP address %s closed for receive error",
					 xConnectedClientData->pcIPStr);
				break;
			}

			LOGI("[EchoServer::%s::%d] message received: %s",
				 xConnectedClientData->pcIPStr,
				 FreeRTOS_ntohs(xConnectedClientData->xSocketInfo.sin_port),
			     pucRxBuffer);

			/* Retransmit the same message. */
			size_t ulTxLength;
			if (bEthTx(xConnectedClientData->xSocket, pucRxBuffer,
					ulRxLength, &ulTxLength) != pdPASS)
			{
				/* Socket closed. */
				LOGW("Socket with IP address %s closed for transmit error: "
					 "transmitted %d bytes of %d",
					 xConnectedClientData->pcIPStr, ulTxLength, ulRxLength);
				break;
			}
		}
	}

	/* Close connection. */
	if (bEthGracefulClose(xConnectedClientData->xSocket,
			echo_serverSHUTDOWN_TIME_MS) == pdFAIL)
	{
		LOGW("Socket with IP address %s not closed gracefully",
			 xConnectedClientData->pcIPStr);
	}

	vEthCommonDeinitData(xConnectedClientData);
	vTaskDelete(NULL);		/*< Kill this task. */
}

/* -------------------------------------------------------------------------- */
BaseType_t vEchoServerCreateTask(void *pvParameters)
{
	/* Build task name. */
	size_t pcTaskNameSize = strlen(pcCommonTaskString(TASK_ECHOSERVER))
							+ strlen(echo_serverNAME_LISTEN_TASK) + 1;
	char *pcTaskName = pvPortMalloc(sizeof(char) * pcTaskNameSize);
	if (!pcTaskName)
	{
		LOGE("Not enough memory for task name allocation");
		return pdFAIL;
	}
	memset(pcTaskName, 0x00, pcTaskNameSize);
	strcat(strcpy(pcTaskName, pcCommonTaskString(TASK_ECHOSERVER)),
		   echo_serverNAME_LISTEN_TASK);

	if (xTaskCreate(prvvEchoServerListeningTask,
					pcTaskName,
					echo_serverTASK_STACK_SIZE,
					pvParameters,
					echo_serverTASK_PRIORITY,
					NULL) != pdPASS)
	{
		LOGE("Not enough memory for %s task", pcTaskName);
		vPortFree(pcTaskName);
		return pdFAIL;
	}

	vPortFree(pcTaskName);
	return pdPASS;
}
