/*
 * echo-client.c
 *
 *  Created on: Nov 14, 2020
 *      Author: mircodemarchi
 *
 * Echo Client implementation.
 */

/* --- Includes ------------------------------------------------------------- */
/* Standard includes. */
#include <stdlib.h>
#include <time.h>

/* Application includes. */
#include "echo-client.h"

/* --- Prototypes ----------------------------------------------------------- */
/**
 * @brief Echo Client task realization.
 * @param pvParameters Parameter for Echo Client task, that in this case is the
 * Echo Server IP address.
 */
static void prvvEchoClientTask(void *pvParameters);

/**
 * @brief Initialize the Echo message to send.
 * @param pcBuffer The buffer to fill with the echo message to send.
 * @param ulLength The length of the buffer.
 * @return The same buffer pointer in input.
 */
static char * prvpcEchoClientInitEchoMessage(char *pcBuffer, size_t ulLength);

/* --- Globals -------------------------------------------------------------- */
SemaphoreHandle_t xEchoClientSemaphore = NULL;
/* -------------------------------------------------------------------------- */

static void prvvEchoClientTask(void *pvParameters)
{
	char * pcRemoteIP = (char *) pvParameters;
	uint32_t ulError = 0, ulCorrect = 0;

	LOGD("Created EchoServer task on %s IP server", pcRemoteIP);

#if echo_clientWITH_SEMAPHORE
	xEchoClientSemaphore = xSemaphoreCreateBinary();
#endif

	/* Create the buffers used to send the string that will be echoed back. */
	uint8_t *pucTxBuffer = (uint8_t *) pvPortMalloc(echo_clientMESSAGE_SIZE);
	if (!pucTxBuffer)
	{
		LOGE("Delete task: not enough heap memory for pucTxBuffer");
		vTaskDelete(NULL);
		return;
	}
	uint8_t *pucRxBuffer = (uint8_t *) pvPortMalloc(echo_clientMESSAGE_SIZE);
	if (!pucRxBuffer)
	{
		LOGE("Delete task: not enough heap memory for pucRxBuffer");
		vTaskDelete(NULL);
		return;
	}

	size_t ulTxLength = echo_clientMESSAGE_SIZE;
	size_t ulRxLength;

	while(1)
	{
		/* Create and bind socket. */
		Socket_t xClient = xEthCreateTCPSocket(
				echo_clientRX_TIMEOUT_MS, echo_clientTX_TIMEOUT_MS,
				echo_clientRX_BUFFER_SIZE, echo_clientTX_BUFFER_SIZE,
				&xEchoClientSemaphore, echo_clientRX_SLIDING_WIN_MSS,
				echo_clientTX_SLIDING_WIN_MSS);
		if (xClient == FREERTOS_INVALID_SOCKET
			|| (bEthConnect(xClient, echo_clientSERVER_PORT_NUMBER, pcRemoteIP)
				!= pdPASS))
		{
			LOGE("Impossible to allocate and bind a new socket");
			vTaskDelete(NULL);
			return;
		}

		ulRxLength = 0;

		/* Perform echo. */
		memset(pucTxBuffer, 0x00, echo_clientMESSAGE_SIZE);
		memset(pucRxBuffer, 0x00, echo_clientMESSAGE_SIZE);

		/* Initialize transmitting message. */
		prvpcEchoClientInitEchoMessage((char *) pucTxBuffer,
				echo_clientMESSAGE_SIZE);
		LOGI("[EchoClient::%s] Message generated: %s", pcRemoteIP, pucTxBuffer);

		/* Transmit a message. */
		size_t ulTransmitted = 0;
		if (bEthTx(xClient, pucTxBuffer, ulTxLength, &ulTransmitted) == pdPASS)
		{
			/* Receive the echo message. */
			while (ulRxLength < ulTxLength)
			{
				size_t ulRxCurrentSize = 0;
				if (bEthRx(xClient, pucRxBuffer + ulRxLength,
						echo_clientMESSAGE_SIZE - ulRxLength,
						&ulRxCurrentSize) == pdFAIL || ulRxCurrentSize < 0)
				{
					/* Error on receive. */
					break;
				}

				ulRxLength += ulRxCurrentSize;
			}

			/* Check message equivalence. */
			if (ulRxLength > 0 &&
				strncmp((char *) pucRxBuffer, (char *) pucTxBuffer,
						echo_clientMESSAGE_SIZE) == 0)
			{
				ulCorrect++;
				LOGI("[EchoClient::%s] Message echoed equal"
					 " (network correct count %d)", pcRemoteIP, ulCorrect);
			}
			else
			{
				ulError++;
				LOGE("[EchoClient::%s] Message echoed not equal"
					 " (network error count %d)", pcRemoteIP, ulError);
			}
		}
		else
		{
			/* Socket closed. */
			LOGW("Socket on %s server IP address closed for transmit error: "
				 "transmitted %d bytes of %d",
				 pcRemoteIP, ulTransmitted, ulTxLength);
		}

		/* Close connection. */
		if (bEthGracefulClose(xClient, echo_clientSHUTDOWN_TIME_MS) == pdFAIL)
		{
			LOGW("Socket on IP address %s server not closed gracefully",
					pcRemoteIP);
		}

		/* Wait some time before restart loop. */
		LOGD("[EchoClient::%s] Wait", pcRemoteIP);
		vTaskDelay(pdMS_TO_TICKS(echo_clientLOOP_DELAY_MS));
	}

	LOGE("Should not arrive here");
	vPortFree(pucTxBuffer);
	vPortFree(pucRxBuffer);
	vTaskDelete(NULL);		/*< Kill this task. */
}

static char * prvpcEchoClientInitEchoMessage(char *pcBuffer, size_t ulLength)
{
	srand(time(NULL));
	for (size_t i = 0; i < ulLength; i++)
	{
		pcBuffer[i] = (rand() % ('z' - 'a')) + 'a';
	}

	pcBuffer[ulLength - 2] = '\n';
	pcBuffer[ulLength - 1] = '\0';

	return pcBuffer;
}

/* -------------------------------------------------------------------------- */
BaseType_t vEchoClientCreateTask(void *pvParameters)
{
	if (xTaskCreate(prvvEchoClientTask,
					pcCommonTaskString(TASK_ECHOCLIENT),
					echo_clientTASK_STACK_SIZE,
					pvParameters,
					echo_clientTASK_PRIORITY,
					NULL) != pdPASS)
	{
		LOGE("Not enough memory for %s task",
			 pcCommonTaskString(TASK_ECHOCLIENT));
		return pdFAIL;
	}

	return pdPASS;
}
