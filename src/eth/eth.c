/*
 * eth.c
 *
 *  Created on: Oct 25, 2020
 *      Author: mircodemarchi
 *
 *
 * Ethernet TCP and UDP Socket creation and setup source code.
 */

/* --- Includes ------------------------------------------------------------- */
/* Application includes. */
#include "eth.h"
/* -------------------------------------------------------------------------- */

void vEthInit()
{
	FreeRTOS_IPInit(ucIPAddress,
					ucNetMask,
					ucGatewayAddress,
					ucDNSServerAddress,
					ucMACAddress);
}

Socket_t xEthCreateTCPSocket(TickType_t xRxTimeoutMS ,TickType_t xTxTimeoutMS,
		int32_t lRxBufferSize, int32_t lTxBufferSize,
		SemaphoreHandle_t *pxSem,
		int32_t lRxSlidingWinMSS, int32_t lTxSlidingWinMSS)
{
	Socket_t xSocket = FreeRTOS_socket(FREERTOS_AF_INET, FREERTOS_SOCK_STREAM,
			FREERTOS_IPPROTO_TCP);

	if (xSocket == FREERTOS_INVALID_SOCKET)
	{
		LOGE("Error socket invalid: not enough heap memory");
		return FREERTOS_INVALID_SOCKET;
	}

	/* Receive and send timeout. */
	if (bEthSetRxTxTimeout(xSocket, xRxTimeoutMS, xTxTimeoutMS) != pdPASS)
	{
		LOGW("Socket send and receive timeout not set properly");
	}

	/* Semaphore referenced with socket. */
	if (bEthSetSemaphore(xSocket, pxSem) != pdPASS)
	{
		LOGW("Socket semaphore not set properly");
	}

	/* Receive and send buffers and sliding windows. */
	if (bEthSetRxTxWinBuffSize(xSocket, lRxBufferSize ,lTxBufferSize,
			lRxSlidingWinMSS, lTxSlidingWinMSS) != pdPASS)
	{
		LOGW("Socket sliding windows not set properly");

		if (bEthSetRxTxBuffSize(xSocket, lRxBufferSize, lTxBufferSize)
				!= pdPASS)
		{
			LOGW("Socket receive and send buffer size not set properly");
		}
	}

	return xSocket;
}

Socket_t xEthCreateUDPSocket(TickType_t xRxTimeoutMS ,TickType_t xTxTimeoutMS,
		BaseType_t xEnableChecksum, BaseType_t ulRxPacketNum)
{
	Socket_t xSocket = FreeRTOS_socket(FREERTOS_AF_INET, FREERTOS_SOCK_DGRAM,
			FREERTOS_IPPROTO_UDP);

	if (xSocket == FREERTOS_INVALID_SOCKET)
	{
		LOGE("Error socket invalid: not enough heap memory");
		return FREERTOS_INVALID_SOCKET;
	}

	/* Receive and send timeout. */
	if (bEthSetRxTxTimeout(xSocket, xRxTimeoutMS, xTxTimeoutMS) != pdPASS)
	{
		LOGW("Socket send and receive timeout not set properly");
	}

	/* Settings for checksum generation. */
	if (bEthSetChecksumGeneration(xSocket, xEnableChecksum) != pdPASS)
	{
		LOGW("Socket checksum setting not set properly");
	}

	/* Maximum number of packet in receive queue. */
	if (bEthSetMaxRxPacket(xSocket, ulRxPacketNum) != pdPASS)
	{
		LOGW("Socket max number of packet on receive not set properly");
	}
	return xSocket;
}

BaseType_t bEthSetRxTxTimeout(Socket_t xSocket, TickType_t xRxTimeoutMS,
		TickType_t xTxTimeoutMS)
{
	xRxTimeoutMS = pdMS_TO_TICKS(xRxTimeoutMS); /*< Converted in ticks. */
	if (FreeRTOS_setsockopt(xSocket,
						    0, /* Parameter not used. */
						    FREERTOS_SO_RCVTIMEO,
						    &xRxTimeoutMS, sizeof(xRxTimeoutMS)) != 0)
	{
		return pdFAIL;
	}

	xTxTimeoutMS = pdMS_TO_TICKS(xTxTimeoutMS); /*< Converted in ticks. */
	if (FreeRTOS_setsockopt(xSocket,
						    0, /* Parameter not used. */
						    FREERTOS_SO_SNDTIMEO,
						    &xTxTimeoutMS, sizeof(xTxTimeoutMS)) != 0)
	{
		return pdFAIL;
	}

	return pdPASS;
}

BaseType_t bEthSetSemaphore(Socket_t xSocket, SemaphoreHandle_t *pxSem)
{
#if ipconfigSOCKET_HAS_USER_SEMAPHORE
	if (FreeRTOS_setsockopt(xSocket,
						    0, /* Parameter not used. */
						    FREERTOS_SO_SET_SEMAPHORE,
						    pxSem, sizeof(SemaphoreHandle_t)) != 0)
	{
		return pdFAIL;
	}

	return pdPASS;
#else
	return pdFAIL;
#endif
}

BaseType_t bEthSetRxTxBuffSize(Socket_t xSocket,
		int32_t lRxBufferSize, int32_t lTxBufferSize)
{
	if (FreeRTOS_setsockopt(xSocket,
						    0, /* Parameter not used. */
						    FREERTOS_SO_RCVBUF,
						    &lRxBufferSize, sizeof(lRxBufferSize)) != 0)
	{
		return pdFAIL;
	}

	if (FreeRTOS_setsockopt(xSocket,
						    0, /* Parameter not used. */
						    FREERTOS_SO_SNDBUF,
						    &lTxBufferSize, sizeof(lTxBufferSize)) != 0)
	{
		return pdFAIL;
	}

	return pdPASS;
}

BaseType_t bEthSetRxTxWinBuffSize(Socket_t xSocket,
		int32_t lRxBufferSize, int32_t lTxBufferSize,
		int32_t lRxSlidingWinMSS, int32_t lTxSlidingWinMSS)
{
#if ipconfigUSE_TCP_WIN
	WinProperties_t xWinProps;
	/* Fill in the required buffer and window sizes. */
	xWinProps.lRxBufSize = lRxBufferSize; 		/*< Unit: bytes. */
	xWinProps.lRxWinSize = lRxSlidingWinMSS;	/*< Unit: MSS. */
	xWinProps.lTxBufSize = lTxBufferSize;		/*< Unit: bytes. */
	xWinProps.lTxWinSize = lTxSlidingWinMSS;	/*< Unit: MSS. */

	if (FreeRTOS_setsockopt(xSocket,
						    0, /* Parameter not used. */
						    FREERTOS_SO_WIN_PROPERTIES,
						    &xWinProps, sizeof(xWinProps)) != 0)
	{
		return pdFAIL;
	}

	return pdPASS;
#else
	return pdFAIL;
#endif
}

BaseType_t bEthSetChecksumGeneration(Socket_t xSocket,
		BaseType_t xEnableChecksum)
{
	if (FreeRTOS_setsockopt(xSocket,
						    0, /* Parameter not used. */
						    FREERTOS_SO_UDPCKSUM_OUT,
						    &xEnableChecksum, sizeof(xEnableChecksum)) != 0)
	{
		return pdFAIL;
	}

	return pdPASS;
}

BaseType_t bEthSetMaxRxPacket(Socket_t xSocket, BaseType_t ulRxPacketNum)
{
#if ipconfigUDP_MAX_RX_PACKETS
	if (FreeRTOS_setsockopt(xSocket,
						    0, /* Parameter not used. */
						    FREERTOS_SO_UDP_MAX_RX_PACKETS,
						    &ulRxPacketNum, sizeof(ulRxPacketNum)) != 0)
	{
		return pdFAIL;
	}

	return pdPASS;
#else
	return pdFAIL;
#endif
}

BaseType_t bEthDisableRxData(Socket_t xSocket)
{
	BaseType_t bValue = pdTRUE;
	if (FreeRTOS_setsockopt(xSocket,
		                    0,
		                    FREERTOS_SO_STOP_RX,
		                    (void *) &bValue,
		                    sizeof(bValue)) != 0)
	{
		return pdFAIL;
	}

	return pdPASS;
}

BaseType_t bEthEnableRxData(Socket_t xSocket)
{
	BaseType_t bValue = pdFALSE;
	if (FreeRTOS_setsockopt(xSocket,
	                    0,
	                    FREERTOS_SO_STOP_RX,
	                    (void *) &bValue,
	                    sizeof(bValue)) != 0)
	{
		return pdFAIL;
	}

	return pdPASS;
}

BaseType_t bEthReuseListening(Socket_t xSocket)
{
	BaseType_t bValue = pdTRUE;
	if (FreeRTOS_setsockopt(xSocket,
		                    0,
							FREERTOS_SO_REUSE_LISTEN_SOCKET,
		                    (void *) &bValue,
		                    sizeof(bValue)) != 0)
	{
		return pdFAIL;
	}

	return pdPASS;
}

BaseType_t bEthCloseOnLastDataSent(Socket_t xSocket)
{
	BaseType_t bValue = pdTRUE;
	if (FreeRTOS_setsockopt(xSocket,
		                    0,
							FREERTOS_SO_CLOSE_AFTER_SEND,
		                    (void *) &bValue,
		                    sizeof(bValue)) != 0)
	{
		return pdFAIL;
	}

	return pdPASS;
}

BaseType_t bEthSendOnlyFullPacket(Socket_t xSocket)
{
	BaseType_t bValue = pdTRUE;
	if (FreeRTOS_setsockopt(xSocket,
		                    0,
							FREERTOS_SO_SET_FULL_SIZE,
		                    (void *) &bValue,
		                    sizeof(bValue)) != 0)
	{
		return pdFAIL;
	}

	return pdPASS;
}

BaseType_t bEthGracefulClose(Socket_t xSocket,
		TickType_t xShutdownDelayMS)
{
	BaseType_t xRet = pdFALSE;
	uint8_t pucTmpBuffer;

	/* Initiate a shutdown in case it has not already been initiated. */
	FreeRTOS_shutdown(xSocket, FREERTOS_SHUT_RDWR);

	/* Wait to receive read attempt, but stop waiting when timer expires. */
	TickType_t xTimeOnShutdown = xTaskGetTickCount();
	do
	{
		if (FreeRTOS_recv(xSocket, &pucTmpBuffer, sizeof(pucTmpBuffer), 0) < 0)
		{
			/* Close loop if received an EINVAL error. */
			xRet = pdTRUE;
			break;
		}
	} while ((xTaskGetTickCount() - xTimeOnShutdown)
			< pdMS_TO_TICKS(xShutdownDelayMS));

	FreeRTOS_closesocket(xSocket);

	return xRet;
}

BaseType_t bEthBind(Socket_t xSocket, uint16_t usPort)
{
	struct freertos_sockaddr xBindAddress;
	xBindAddress.sin_port = FreeRTOS_htons(usPort);

	if (FreeRTOS_bind(xSocket, &xBindAddress, sizeof(xBindAddress)) == 0)
	{
		return pdPASS;
	}

	return pdFAIL;
}

BaseType_t bEthConnect(Socket_t xSocket, uint16_t usPort, char *pcIP)
{
	struct freertos_sockaddr xBindAddress;
	xBindAddress.sin_port = FreeRTOS_htons(usPort);
	xBindAddress.sin_addr = FreeRTOS_inet_addr(pcIP);

	if (FreeRTOS_connect(xSocket, &xBindAddress, sizeof(xBindAddress)) == 0)
	{
		return pdPASS;
	}

	return pdFAIL;
}

BaseType_t bEthTx(Socket_t xSocket, const uint8_t *pcBuffer, size_t ulLength,
		size_t *pulBytesTransmitted)
{
	size_t ulAlreadyTransmitted = 0;

    /* Keep sending until the entire buffer has been sent. */
    while (ulAlreadyTransmitted < ulLength)
    {
        BaseType_t xTransmitted = FreeRTOS_send(xSocket,
        		pcBuffer + ulAlreadyTransmitted, /* Buffer of remaining data. */
        		ulLength - ulAlreadyTransmitted, /* Size of remaining data. */
				0);						  		 /* Parameter not used. */

        if (xTransmitted >= 0)
        {
            /* Data was sent successfully. */
        	ulAlreadyTransmitted += xTransmitted;
        }
        else
        {
            /* Error. */
        	if (pulBytesTransmitted)
			{
        		*pulBytesTransmitted = ulAlreadyTransmitted;
			}
            return pdFAIL;
        }
    }

    if (pulBytesTransmitted)
    {
    	*pulBytesTransmitted = ulAlreadyTransmitted;
    }
    return pdPASS;
}

BaseType_t bEthRx(Socket_t xSocket, uint8_t *pcBuffer, size_t ulLength,
		size_t *pulBytesReceived)
{
	BaseType_t xReceived = FreeRTOS_recv(xSocket,
			pcBuffer,	/* Buffer for receiving data. */
			ulLength,	/* Max size of buffer. */
			0);			/* Parameter not used. */

	if (xReceived > 0)
	{
		/* Data was received successfully. */
		if (pulBytesReceived)
		{
			*pulBytesReceived = xReceived;
		}
		return pdPASS;
	}
	else if (xReceived == 0)
	{
		/* Receiving timeout expired. */
		if (pulBytesReceived)
		{
			*pulBytesReceived = 0;
		}
		return pdPASS;
	}
	else
	{
		/* Error. */
		return pdFAIL;
	}
}
