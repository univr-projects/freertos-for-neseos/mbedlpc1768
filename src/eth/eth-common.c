/*
 * eth-common.c
 *
 *  Created on: Oct 25, 2020
 *      Author: mircodemarchi
 *
 * Ethernet common data and utils implementation file.
 */


/* --- Includes ------------------------------------------------------------- */
/* Application includes. */
#include "eth-common.h"

/* --- Globals -------------------------------------------------------------- */
/**
 * @brief The MAC address array is not declared const as the MAC address will
 * normally be read from an EEPROM and not hard coded.
 * TODO: read MAC address from an EEPROM.
 */
uint8_t ucMACAddress[eth_commonMAC_ADDRESS_LEN] = {
		configMAC_ADDR0, configMAC_ADDR1, configMAC_ADDR2,
		configMAC_ADDR3, configMAC_ADDR4, configMAC_ADDR5
};

/**
 * @brief Define the network addressing. These parameters will be used if
 * either ipconfigUDE_DHCP is 0 or if ipconfigUSE_DHCP is 1 but DHCP auto
 * configuration failed.
 */
const uint8_t ucIPAddress[eth_commonIP_ADDRESS_LEN] 		= {
		configIP_ADDR0, configIP_ADDR1, configIP_ADDR2, configIP_ADDR3
};
const uint8_t ucNetMask[eth_commonIP_ADDRESS_LEN] 			= {
		configNET_MASK0, configNET_MASK1, configNET_MASK2, configNET_MASK3
};
const uint8_t ucGatewayAddress[eth_commonIP_ADDRESS_LEN] 	= {
		configGWY_ADDR0, configGWY_ADDR1, configGWY_ADDR2, configGWY_ADDR3
};

/* @brief The following is the address of an OpenDNS server. */
const uint8_t ucDNSServerAddress[eth_commonIP_ADDRESS_LEN] = {
		configDNS_ADDR0, configDNS_ADDR1, configDNS_ADDR2, configDNS_ADDR3
};
/* -------------------------------------------------------------------------- */

char *pcEthCommonIpToStr(char *pcBuffer, uint32_t ulIP)
{
	FreeRTOS_inet_ntoa(ulIP, pcBuffer);
	return pcBuffer;
}

EthCommonData_t *vEthCommonInitData(Socket_t xSocket,
		struct freertos_sockaddr xSocketInfo)
{
	EthCommonData_t *pxRet = pvPortMalloc(sizeof(EthCommonData_t));
	if (!pxRet) return NULL;
	pxRet->xSocket = xSocket;
	pxRet->xSocketInfo = xSocketInfo;
	pxRet->pcIPStr = pvPortMalloc(sizeof(char) * eth_commonIP_ADDRESS_STR_LEN);
	if (!pxRet->pcIPStr)
	{
		vPortFree(pxRet);
		return NULL;
	}
	memset(pxRet->pcIPStr, 0x00, eth_commonIP_ADDRESS_STR_LEN);
	pcEthCommonIpToStr(pxRet->pcIPStr, xSocketInfo.sin_addr);
	return pxRet;
}

void vEthCommonDeinitData(EthCommonData_t *pxData)
{
	vPortFree(pxData->pcIPStr);
	vPortFree(pxData);
}




