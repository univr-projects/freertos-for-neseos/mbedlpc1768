# FreeRTOS for NESEOS with mbed NXP LPC1768

Network Embedded System (NES) and Embedded Operating System (EOS) project concerning FreeRTOS on a mbed NXP LPC1768 platform.

## Usefull links

- [NXP official webpage](https://www.nxp.com);
- [NXP LPC1768 Homepage](https://www.nxp.com/products/processors-and-microcontrollers/arm-microcontrollers/general-purpose-mcus/lpc1700-cortex-m3/512kb-flash-64kb-sram-ethernet-usb-lqfp100-package:LPC1768FBD100);
- [MCUXpresso IDE](https://www.nxp.com/design/software/development-software/mcuxpresso-software-and-tools-/mcuxpresso-integrated-development-environment-ide:MCUXpresso-IDE);
- [LPCOpen for LPC17XX](https://www.nxp.com/design/microcontrollers-developer-resources/lpcopen-libraries-and-examples/lpcopen-software-development-platform-lpc17xx:LPCOPEN-SOFTWARE-FOR-LPC17XX)
- [ARM mbed official website](https://os.mbed.com);
- [mbed NXP LPC1769 Quick Start](https://os.mbed.com/platforms/mbed-LPC1768/);
- [Firmware upgrade](https://os.mbed.com/handbook/Firmware-LPC1768-LPC11U24) (the latest firmware version is 141212);
- [FreeRTOS official website](https://www.freertos.org);

## mbed NXP LPC1768
- High performance ARM® Cortex™-M3 Core
- 96MHz, 32KB RAM, 512KB FLASH
- Ethernet, USB Host/Device, 2xSPI, 2xI2C, 3xUART, CAN, 6xPWM, 6xADC, GPIO
- 40-pin 0.1" pitch DIP package, 54x26mm
- 5V USB or 4.5-9V supply
- Built-in USB drag&drop FLASH programmer

## ARM mbed OS and tools

The mbed Software Development Kit (SDK) is an open source C/C++ microcontroller software platform. The mbed SDK has been designed to provide enough hardware abstraction to be intuitive and concise, yet powerful enough to build complex projects.

The mbed Hardware Development Kit (HDK) provides full microcontroller sub-system design files and firmware for building development boards and custom product. The HDK specifies all support components and circuits including the mbed Onboard Interface design.

The LPC1768 platform can be easily integrated with mbed OS using the official mbed device development tools as the [Mbed Online Compiler](https://os.mbed.com/docs/mbed-os/v6.2/quick-start/build-with-the-online-compiler.html) (highly recommended from the mbed mantainer), [Mbed Studio](https://os.mbed.com/studio/) or [Mbed CLI](https://os.mbed.com/docs/mbed-os/v6.2/quick-start/build-with-mbed-cli.html). There is an unofficial tool, widely used, for mbed device development, that is the [PlatformIO](https://platformio.org) extension for [Visual Studio Code](https://code.visualstudio.com).

The mbed OS has available some FreeRTOS libraries that can be included from the mbed development tools, but all of these cannot be compiled for our LPC1768. I have tried all the development enviroment listed above, but none of these work. This is the reason why I change the development tools in NXP MCUXpresso.

## MCUXpresso IDE and LPCOpen libraries

The alternative of mbed tools are the NXP MCUXpresso solution, that is the solution adopted for this project.

The MCUXpresso IDE brings developers an easy-to-use Eclipse-based development environment for NXP MCUs based on ARM Cortex-M cores. Download MCUXpresso IDE from this [ide-download-link](https://www.nxp.com/design/software/development-software/mcuxpresso-software-and-tools/mcuxpresso-integrated-development-environment-ide:MCUXpresso-IDE) and install with the downloaded installer;

The LPC platform series api are not included in MCUXpresso SDK, but you can find it included in the MCUXpresso IDE installation folder and you can create your project from these libraries. For example this project has been created from one of the LPCOpen libraries example project. The LPC example projects can be included in the MCUXpresso IDE with the following steps:
1. Open *MCUXpresso IDE*, launch your workspace and go to IDE view.
2. From the *Quickstart* panel in the botton left of the view, click on *Import project(s) from file system...*
3. From the *Project archive (zip)* section click on *Browse...*
4. It will be opened the MCUXpresso IDE installation *Example* folder and, from this folder, open the *LPCOpen* folder.
5. Double click the *lpcopen_2_10_lpcxpresso_nxp_lpcxpresso_1769.zip*.
6. Click on *Next* and select mandatorily the *lpc_board_nxp_lpcxpresso_1769* project and the *lpc_chip_175x_6x* project, that are the LPC libraries, then select the project that you would like to test.
7. Click on "Finish". 

In this project the *lpc_board_nxp_lpcxpresso_1769* and the *lpc_chip_175x_6x* libraries are already included in the [lib](./lib) folder.

# Configuration for Developer

## Project configuration

The setup passages:
1. Clone this repo;
2. Open MCUXpresso, open a workspace and go to the IDE view;
3. From the Quickstart panel choose *Import project(s) from file system...*;
4. Fill the *Root Directory* form with the path of the project cloned;
5. Click on *Next* and disable the option *Copy project into workspace*, otherwise you won't be able to update your project with git;
6. Click on *Finish*;

Now you could be able to compile your project.

## Platform firmware upgrade
The latest firmware version is *141212*. To check your firmware version, open the *MBED.HTM* file in the devide driver of your mbed microcontroller when connected to your PC with a mini-USB cable.

1. Download the latest firmware from the [mbed firmware download link](https://os.mbed.com/handbook/Firmware-LPC1768-LPC11U24).
2. Connect a mini-USB cable to your PC and to the platform. Your board should be now visible as a device drive.
3. Drag the upgrade S-record file with the `.if` file extension previously downloaded onto the device drive.
4. Power-cycle the board bt disconnecting and reconnecting the cable.
5. The platform firmware should now be upgraded.

## Debug firmware with OpenSDA
Follow the steps in [Firmware upgrade](#Firmware\ upgrade) section if the following debug firmware steps do not work. The firmware upgrade tested is the *141212* version.

The driver recognized from MCUXpresso IDE is the mbed LinkServer CMSIS-DAP.

1. Connect the platform to your PC with a mini-USB cable.
2. Open *MCUXpresso IDE*, launch your workspace and go to IDE view.
3. From the top menu, click on *Run* -> *Debug Configurations...* -> *C/C++ (NXP Semiconductors) MCU Application* and create a new configuration with double click.
4. Click on *Apply* and *Debug* to start a new debug session.
5. Wait that a *Probes discovered* window opened and select the available attached probe.
6. Click *OK* and let's debug.

If no available attached probes should be showed in the window, it means that the platform is not correctly attached with the mini-USB cable or the platform firmware is not upgraded. Check the section above to [upgrade the platform firmware](#Platform\ firmware\ upgrade).
